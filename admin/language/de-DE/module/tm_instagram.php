<?php
// Heading
$_['heading_title']       = 'TemplateMonster Instagram';

// Text
$_['text_module']         = 'Module';
$_['text_success']        = 'Erfolg: Sie haben das Templatemonster Instagram Modul verändert!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

$_['text_preview']        = 'Vorschau';

// Entry
$_['entry_get']           = 'Get auswählen';
$_['text_user']           = 'Benutzer';
$_['text_tagged']         = 'markiert';
$_['entry_tag_name']      = 'Tagname';
$_['entry_clientid']      = 'Klient Id';
$_['entry_user_id']       = 'Benutzer Id';
$_['entry_accesstoken']   = 'Zugriff erhalten';
$_['entry_limit']         = 'Limit';
$_['entry_name']          = 'Modulename';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['text_edit']           = 'TemplateMonster Instagram Modul ändern';


// Error
$_['error_tag_name']      = 'Fehler';


$_['error_permission']    = 'Achtung: Sie haben keine Berechtigung das Templatemonster Instagram Modul zu ändern !';
$_['error_name']          = 'Modulname muss 3 bis 64 Zeichen haben!';
$_['error_profile_id']    = 'Profil ID erforderlich';
$_['error_page_url']      = 'Seiten URL erforderlich';
$_['error_connections']   = 'Verbindung erforderlich';
$_['error_dimension']     = 'Größe erforderlich';
$_['error_image']         = 'Bild erforderlich';
$_['error_width']         = 'Breite erforderlich!';
$_['error_height']        = 'Höhe erforderlich!';
$_['error_limit']         = 'Grenze muss numerisch sein';
$_['error_tag']           = 'TagName muss leer sein, nicht';