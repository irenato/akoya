<?php
class ControllerModuleTmInstagram extends Controller {
	public function index($setting) {
		$this->load->language('module/tm_instagram');
		$this->document->addScript('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/js/instagram/instafeed.min.js');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['get']           = $setting['get'];
		$data['tag_name']      = $setting['tag_name'];
		$data['user_id']       = $setting['user_id'];
		$data['accesstoken']   = $setting['accesstoken'];
		$data['limit']         = $setting['limit'];

		return $this->load->view('module/tm_instagram', $data);
	}
}