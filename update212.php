﻿<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
setlocale (LC_ALL, 'nl_NL'); // Преобразуем каракули в кириллицу

/*
*
*  Настройки
*
*/

$options = array(
    'enable'        => true, // Скрипт работает только если значение TRUE
    /* Настройки CSV */
    'filename'      => 'prodimport.csv', // Имя файла CSV. Находиться должен в одной папке со скриптом
    'delimiter'     => ';', // Какой разделитель используется
    /* Настройки подключения к БД */
    'db_server'     => 'akoya.mysql.ukraine.com.ua', // Сервер БД
    'db_user'       => 'akoya_db', // Имя пользователя
    'db_password'   => 'aeqlakmg', // Пароль
    'db_base'       => 'akoya_db' // Имя базы данных

    );

if(!$options['enable']) die('Скрипт отключен, дальнейшая обработка данных невозможна!');

/*
*
*  Функции скрипта
*
*/

// Основная функция, из импортируемого файла выбираем данные в массив
// !Во время первой итерации значения первой строки будут являться ключами ассоциативного массива!

function csv_to_array($filename='') {
    if(!file_exists($filename) || !is_readable($filename)){
        return FALSE;
    }
    global $options;
    $header = array();
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE) {
        while (($row = fgetcsv($handle, 1000, $options['delimiter'])) !== FALSE) {
            if(!$header)
//foreach($row as $r){$header[] = preg_replace("/[^\w\d]/","",trim(strtolower($r)));}
                $header = $row;

            else
               // $data[] = array_combine($header, $row);
$data[] = $row;
        }
        fclose($handle);
    }
    return $data;
}

/*function csv_to_array($filename = '', $delimiter = ';', $asHash = true) {
    if (!(is_readable($filename) || (($status = get_headers($filename)) && strpos($status[0], '200')))) {
        return FALSE;
    }

    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE) {
        if ($asHash) {
            while ($row = fgetcsv($handle, 0, $delimiter)) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
        } else {
            while ($row = fgetcsv($handle, 0, $delimiter)) {
                $data[] = $row;
            }
        }

        fclose($handle);
    }

    return $data;
}*/

// Используется, если необходимо убрать ВСЕ (!!!) пробелы из строки
// Например цена в виде Х ХХХ, а в базе нужно ХХХХ

function space_off($str) {
    if(!empty($str)) {
        return str_replace(" ", "", $str);
    }
    else {
        return FALSE;
    }
}


/*
*
*  / Функции скрипта
*
*  Подключаемся к Базе Данных
*
*/

$link = mysqli_connect($options['db_server'], $options['db_user'], $options['db_password'], $options['db_base']);

if (!$link) {
    die('Ошибка соединения: ' . mysqli_error());
}

// Указываем, что общаемся с БД только в UTF-8

mysqli_query($link, "SET NAMES 'utf8'");
mysqli_query($link, "SET CHARACTER SET 'utf8'");
mysqli_query($link, "SET SESSION collation_connection = 'utf8_general_ci'");

// Выбираем интересующую нас Базу

/*$db_selected = mysqli_select_db($options['db_base'], $link);
if (!$db_selected) {
    die ('Не удалось выбрать базу db_data: ' . mysql_error());
}*/

// Отключаем индексацию таблицы, для максимального быстродействия

mysqli_query($link, "ALTER TABLE `".$options['db_base']."` DISABLE KEYS");
//print_r (csv_to_array($options['filename']));
foreach (csv_to_array($options['filename']) as $val) {
// Тут собственно делаем запросы в соответствии с задачей

      //if ($val[2]=="0"){
		//mysqli_query($link,"UPDATE `oc_product` SET `quantity` = '0' WHERE `model`= $val[0]");echo ($val[0].' - нет на складе</ br>');

	//} else {
       mysqli_query($link,"UPDATE `oc_product` SET `quantity` = $val[2], `price` = $val[1] WHERE `model`= $val[0]");echo ($val[0].' - позиция обновлена<br />');
	//}
	/*elseif ($val[2]=="1"){
		mysqli_query("UPDATE `oc_product` SET `quantity` = '100' WHERE `model`= $val[0]");
		}*/

        //print_r ($val);
        //var_dump ($val['model']);
	//foreach($val as $k => $v) echo '>'.$k.'< => '.$val[$k].'<br />';
	//var_dump($val);
        //echo ($val[0].' '.$val[1].' '.$val[2].'<br />');
}

// Включаем индексацию таблицы

mysqli_query($link, "ALTER TABLE `".$options['db_base']."` ENABLE KEYS");

// Закрываем соединение с БД

mysqli_close($link);

?>