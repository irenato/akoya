<?php
/**
 * @version		$Id: fraudlabspro.php 4037 2015-08-23 15:49:05Z mic $
 * @package		German Translation Backend
 * @author		mic - http://osworx.net
 * @copyright	2015 OSWorX - http://osworx.net
 * @license		OCL - OSWorX Commercial License
 */

// Heading
$_['heading_title']                           = 'FraudLabs Pro';

// Text
$_['text_fraud']                              = 'Betrugsvermeidung';
$_['text_success']                            = 'Modul erfolgreich bearbeitet';
$_['text_edit']                               = 'Einstellungen';
$_['text_signup']                             = 'FraudLabsPro is a fraud detection service. If you don\'t have a API key you can <a href="http://www.fraudlabspro.com/" target="_blank"><u>sign up here</u></a>.';
$_['text_id']                                 = 'FraudLabs Pro ID';
$_['text_ip_address']                         = 'IP Address';
$_['text_ip_net_speed']                       = 'IP Net Speed';
$_['text_ip_isp_name']                        = 'IP ISP Name';
$_['text_ip_usage_type']                      = 'IP Usage Type';
$_['text_ip_domain']                          = 'IP Domain';
$_['text_ip_time_zone']                       = 'IP Time Zone';
$_['text_ip_location']                        = 'IP Location';
$_['text_ip_distance']                        = 'IP Distance';
$_['text_ip_latitude']                        = 'IP Latitude';
$_['text_ip_longitude']                      