<footer>
    <div class="logo_footer">
        <a href="./">
            <img src="image/catalog/logo_footer.png" alt="">
        </a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-3 col-md-3">
                <?php if ($informations) { ?>
                    <div class="footer_box">
                        <h5><?php echo $text_information; ?></h5>
                        <ul class="list-unstyled">
                            <?php foreach ($informations as $information) { ?>
                                <li>
                                    <a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="footer_box">
                    <h5><?php echo $text_service; ?></h5>
                    <ul class="list-unstyled">
                        <li>
                            <a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo $return; ?>"><?php echo $text_return; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="footer_box">
                    <h5><?php echo $text_extra; ?></h5>
                    <ul class="list-unstyled">
                        <li>
                            <a href="<?php echo $special; ?>"><?php echo $text_special; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="footer_box">
                    <h5><?php echo $text_account; ?></h5>
                    <ul class="list-unstyled">
                        <li>
                            <a href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo $order; ?>"><?php echo $text_order; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


	<div class="container">
<div class="col-sm-12 contact_footer">
	<div class="panel panel-default">
		<div class="panel-body">	
		<div class="row">
				<div class="col-xs-6 col-sm-4 col-md-4">
				<div class="icon fa-phone">
					<strong>Телефон опт:</strong><br><a href="callto:+38(098)-485-86-85">+38(098)-485-86-85</a></div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4">
				<div class="icon fa-phone">
					<strong>Телефон розница:</strong><br><a href="callto:+38(099)-315-55-40">+38(099)-315-55-40</a></div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4">
				<div class="icon fa-phone">
					<strong>Viber:</strong><br><a href="callto:+38(063)-400-90-20">+38(063)-400-90-20</a></div>
				</div>
		</div>
	</div>
	<div class="container">
		<div class="panel-body">
		<div class="row">
			<div class="col-sm-12">
				 <!-- <div class="icon fa-key">
					<strong>Режим работы</strong><br>7 дней в неделю с 8:00 до 18:00, кроме выходных и праздничных дней.
				</div> -->
			</div>
		</div>
		</div>
	</div>
</div>
	</div>

    <div class="container">
        <div class="info-block">
<!-- /*             <dl>
                <dt>Tel:</dt>
                <dd>
                    <a href="callto:<?php echo $telephone; ?>">
                        <?php echo $telephone; ?>
                    </a>
                </dd>
            </dl> */-->
           <!-- <address><?php echo $address; ?></address>
            <?php if ($footer_top) { ?>
                <div class="footer">
                        <?php echo $footer_top; ?>
                </div>
            <?php } ?>
            -->
        </div>
        <div class="copyright">
            <?php echo $powered; ?><!-- [[%FOOTER_LINK]] -->
        </div>
    </div>
</footer>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/livesearch.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/script.js" type="text/javascript"></script>
</div>

<div class="ajax-overlay"></div>

</body></html>