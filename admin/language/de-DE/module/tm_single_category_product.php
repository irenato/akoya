<?php
// Heading
$_['heading_title']        = 'TemplateMonster Single Category Products';

// Text
$_['text_module']          = 'Module';
$_['text_success']         = 'Erfolg: Sie haben das Templatemonster Single Category Products Modul verändert!';
$_['text_edit']            = 'TemplateMonster Single Category Products ändern';
$_['text_none']            = 'Kein(e)';
$_['text_layout_carousel'] = 'Karussel';
$_['text_layout_static']   = 'Statisch';

// Entry
$_['entry_name']           = 'Modulename';
$_['entry_product']        = 'Ausgewählte Artikel';
$_['entry_featured']       = 'Empfohlene';
$_['entry_special']        = 'Angebote';
$_['entry_bestseller']     = 'Bestseller';
$_['entry_latest']         = 'Neueste';
$_['entry_limit']          = 'Limit';
$_['entry_width']          = 'Breite';
$_['entry_height']         = 'Höhe';
$_['entry_status']         = 'Status';
$_['entry_tabs']           = 'Tabs';
$_['entry_products']       = 'Produkttyp';
$_['entry_category']       = 'Kategorie';
$_['entry_layout_type']    = 'Layouttyp';

// Help
$_['help_product']         = '(Automatische Vervollständigung)';

// Error
$_['error_permission']     = 'Achtung: Sie haben keine Berechtigung das Templatemonster Single Category Products zu ändern!';
$_['error_name']           = 'Modulname muss 3 bis 64 Zeichen haben!';
$_['error_width']          = 'Breite erforderlich!';
$_['error_height']         = 'Höhe erforderlich!';